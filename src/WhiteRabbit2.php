<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */

    public function findCashPayment($amount){

        /**
         * I start by defining the variables I will use to solve this problem. These each represent a type of
         * coin that is usable in this transaction.
         * Their values are set to zero, as we are asked to asses the proper way to pay a specific amount using
         * the least amount of coins.
         */
        $one = 0;
        $two = 0;
        $five = 0;
        $ten = 0;
        $twenty = 0;
        $fifty = 0;
        $hundred = 0;

        /**
         * Here I state that as long as the amount we have been asked to asses how to pay is not equal to 0
         * then there is still work to be done. Which also ensures, that if the amount is set to 0, it will
         * automatically return the empty array. As we are told to pay with the least amount of coins, I start
         * by using modulo on the type of coin with the highest value. When using modulo I can test wether or 
         * not the amount is the same after using modulo. If it is the same, it means the remainder is the same 
         * as the dividend, which means I cannot divide the amount with that type of coin. I can then move on
         * and test if it is possible with the next type of coin.
         * If the next one is possible, then the type of coin has its variable amount increased by 1, so I know
         * one of them has been used. It is then important to change the value of the amount with the same
         * amount as the value of the type of coin as I in theory have just payed with that type of coin.
         */
        while ($amount > 0){
            if ($amount % 100 != $amount){
                $hundred++;
                $amount -= 100;
            } elseif ($amount % 50 !=$amount){
                $fifty++;
                $amount -= 50;
            } elseif ($amount % 20 !=$amount) {
                $twenty++;
                $amount -= 20;
            } elseif ($amount % 10 !=$amount) {
                $ten++;
                $amount -= 10;
            } elseif ($amount % 5 !=$amount) {
                $five++;
                $amount -= 5;
            } elseif ($amount % 2 !=$amount) {
                $two++;
                $amount -= 2;
            } elseif ($amount % 1 !=$amount) {
                $one++;
                $amount -= 1;
            }
        }
            
        return array(
                '1'   => $one,
                '2'   => $two,
                '5'   => $five,
                '10'  => $ten,
                '20'  => $twenty,
                '50'  => $fifty,
                '100' => $hundred
            );

    }

}