<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        /**
         * This assignment was very open. I looked at the individual components but refrained from using
         * characters as that seemed pointless in this assignement. I hope that was understood correctly.
         */
        return array(
            /**array(9223371733300202500, 303700450, 303700450) maybe not a fair one, although a calculator
             * from the real world probably should take the limit of 64 bit in to consideration for example
             * by using x * 10^y.
             */
            array(49, 7, 7), //Noticed a random integer being removed from the guess variable.
            array(2.94, 0.49, 6), //See below...
            array(2.88, 0.48, 6), //I think the point is made at that point.
            array(ab, a, b), //Is prolly not nice to bring algebra in to this. If that was not allowed I am sorry.
            array(1.25, 5, 0.25) //The amount subtracted from the estimated result had to be bigger than 0.49

        );
    }
}
